       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA4.
       AUTHOR. Puchong.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 STUDENT-REC-DATA PIC x(44) VALUE "1205621WIlliam  Fitzpatrick
      -    "19751021LM051385".
       01 LONG-STR PIC x(200) VALUE "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      -    "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
      -    "xxxxxxx".
       01 STUDENT-REC.
           05 STUDENT-ID     PIC 9(7).
           05 STUDENT-NAME.
              10 FORENAME    PIC x(9).
              10 SURNAME.
                 15 F-SURNAME   PIC X.
                 15 FILLER      PIC x(11).
           05 DATE-OF-BIRTH.
              08 YOB         PIC 9(4).
              08 MOB-DOB.
                 10 MOB         PIC 99.
                 10 DOB         PIC 99.
           05 COURSE-ID      PIC x(5).
           05 GPA            PIC 9v99.

       PROCEDURE DIVISION.
           DISPLAY STUDENT-REC-DATA 
           MOVE STUDENT-REC-DATA TO STUDENT-REC.
           DISPLAY STUDENT-REC
           DISPLAY STUDENT-ID 
           
           DISPLAY STUDENT-NAME
           DISPLAY FORENAME 
           DISPLAY SURNAME 
           DISPLAY F-SURNAME "." FORENAME
           
           DISPLAY DATE-OF-BIRTH 
           DISPLAY DOB"/"MOB"/"YOB

           DISPLAY COURSE-ID 
           DISPLAY GPA 

           DISPLAY MOB-DOB
           .
 